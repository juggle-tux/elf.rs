use {Addr32, Addr64};
use nom::{ErrorKind, IResult};
use parser::{Address, DataEnc, Name, Parser, ParserError, byte};
use std::any::TypeId;
use std::fmt;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Symbol<A>
    where A: Address
{
    pub name: Name,
    pub value: A,
    pub size: A,
    pub info: u8,
    pub other: u8,
    pub section_idx: u16,
}

impl<A: Address> Parser for Symbol<A> {
    fn parse(i: &[u8], enc: DataEnc) -> IResult<&[u8], Self, ParserError> {
        match TypeId::of::<A>() {
            ty if ty == TypeId::of::<Addr32>() => parse_symbol32(i, enc),
            ty if ty == TypeId::of::<Addr64>() => parse_symbol64(i, enc),
            _ => unimplemented!(),
        }
    }
}

fn parse_symbol32<A: Address>(i: &[u8], enc: DataEnc) -> IResult<&[u8], Symbol<A>, ParserError> {
    add_return_error!(i,
        ErrorKind::Custom(ParserError::Symbol),
        do_parse!(
            name: apply!(Name::parse, enc) >>
            value: apply!(A::parse, enc) >>
            size: apply!(A::parse, enc) >>
            info: byte >>
            other: byte >>
            section_idx: apply!(u16::parse, enc) >>
            (
                Symbol {
                    name,
                    value,
                    size,
                    info,
                    other,
                    section_idx,
                }
            )
        )
    )
}

fn parse_symbol64<A: Address>(i: &[u8], enc: DataEnc) -> IResult<&[u8], Symbol<A>, ParserError> {
    add_return_error!(i,
        ErrorKind::Custom(ParserError::Symbol),
        do_parse!(
            name: apply!(Name::parse, enc) >>
            info: byte >>
            other: byte >>
            section_idx: apply!(u16::parse, enc) >>
            value: apply!(A::parse, enc) >>
            size: apply!(A::parse, enc) >>
            (
                Symbol {
                    name,
                    value,
                    size,
                    info,
                    other,
                    section_idx,
                }
            )
        )
    )
}

impl<A> fmt::Display for Symbol<A>
    where A: Address
{
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt,
               "{value:#x}\tsize: {size} bytes\n\
                     info {info:#x}\t other: {other:#x}\n\
                     section index: {shndx}",
               value = self.value,
               size = self.size,
               info = self.info,
               other = self.other,
               shndx = self.section_idx)
    }
}
