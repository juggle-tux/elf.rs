use nom::{ErrorKind, IResult};
use parser::{ParserError, byte};
use std::fmt;

/// The `Ident` struct specifies how to interpret the file, independent
/// of the processor or the file's remaining contents.
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Ident {
    pub class: Class,
    pub data_enc: DataEnc,
    pub elf_version: Version,
    pub os_abi: Abi,
}

impl Ident {
    named!(pub parse<&[u8], Self, ParserError>,
        add_return_error!(ErrorKind::Custom(ParserError::Ident),
            do_parse!(
                fix_error!(ParserError, tag!("\x7fELF")) >>
                class: call!(Class::parse) >>
                data_enc: call!(DataEnc::parse) >>
                elf_version: call!(Version::parse) >>
                os_abi: call!(Abi::parse) >>
                fix_error!(ParserError, take!(7)) >>
                (
                    Ident{
                        class,
                        data_enc,
                        elf_version,
                        os_abi,
                    }
                )
            )
        )
    );
}

impl fmt::Display for Ident {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt,
               "{class}\n\
                encoding: {enc}\n\
                version: {ver}\n\
                abi: {abi}",
               class = self.class,
               enc = self.data_enc,
               ver = self.elf_version,
               abi = self.os_abi)
    }
}

with_parser!(
    #[doc = "The `Class` identifies the architecture for this binary"]
    enum Class where @ _ => Err(ParserError::Class) {
        1 => Elf32,
        2 => Elf64,
    }
);

impl fmt::Display for Class {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{:?}", self)
    }
}

with_parser!(
    #[doc = "The `DataEnc` specifies the data encoding of the processor-specific data in the file."]
    enum DataEnc where @ _ => Err(ParserError::DataEnc) {
        1 => LittleEndian,
        2 => BigEndian,
    }
);

impl fmt::Display for DataEnc {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            DataEnc::LittleEndian => write!(fmt, "little endian"),
            DataEnc::BigEndian => write!(fmt, "big endian"),
        }
    }
}

with_parser!(
    #[doc = "The `Version` is the version number of the ELF specification."]
    enum Version where @ _ => Err(ParserError::Version) {
        1 => Current,
    }
);
impl_parser!(
    for Version(u32::parse) where @ _ => Err(ParserError::Version) {
        1 => Current,
    }
);

impl fmt::Display for Version {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{:?}", self)
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Abi {
    pub kind: AbiKind,
    pub version: AbiVersion,
}

impl Abi {
    named!(parse<&[u8], Abi, ParserError>,
        do_parse!(
            kind: call!(AbiKind::parse) >>
            version: byte >>
            (
                Abi{
                    kind,
                    version
                }
            )
        )
    );
}

impl fmt::Display for Abi {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{}\tversion: {}", self.kind, self.version)
    }
}

with_parser!(
    #[doc = "The `AbiKind` identifies the operating system and ABI to which the object is targeted."]
    enum AbiKind where Err(ParserError::AbiKind) {
        0 => SYSV,
        1 => HPUX,
        2 => NETBSD,
        3 => LINUX,
        6 => SOLARIS,
        7 => AIX,
        8 => IRIX,
        9 => FREEBSD,
        10 => TRU64,
        11 => MODESTO,
        12 => OPENBSD,
        64 => ARMeabi,
        97 => ARM,
        255 => STANDALONE,
        _ => Unknown(u8),
    }
);

impl fmt::Display for AbiKind {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{:?}", self)
    }
}

/// The `AbiVersion` identifies the version of the ABI to which the object
/// is targeted.
///
/// This field is used to distinguish among incompatible versions of
/// an ABI. The interpretation of this version number is dependent on
/// the ABI identified by the `Ident.os_abi` field. Applications
/// conforming to this specification use the value 0.
pub type AbiVersion = u8;

#[cfg(test)]
mod tests {
    use nom::IResult;
    use parser::ParserError;
    use super::{Abi, AbiKind, Class, DataEnc, Ident, Version};

    pub struct Test {
        pub raw: &'static [u8],
        pub want: IResult<&'static [u8], Ident, ParserError>,
    }

    pub const TEST_IDENT: Test = Test{
        raw: include_bytes!(concat!(env!("CARGO_MANIFEST_DIR"), "/test_data/ident.bin")),
        want: IResult::Done(&[], Ident {
            class: Class::Elf64,
            data_enc: DataEnc::LittleEndian,
            elf_version: Version::Current,
            os_abi: Abi {
                kind: AbiKind::SYSV,
                version: 0,
            },
        })
    };

    #[test]
    fn parse_ident() {
        let (_, got) = Ident::parse(TEST_IDENT.raw).unwrap();
        let (_, want) = TEST_IDENT.want.unwrap();
        assert_eq!(want, got);
    }
}

#[cfg(all(test, feature="benchmark"))]
mod benchmarks {
    use super::Ident;
    use super::tests::TEST_IDENT;
    use test::Bencher;

    #[bench]
    fn parsing_ident(b: &mut Bencher) {
        b.bytes = ident_size!();
        b.iter(|| Ident::parse(TEST_IDENT.raw).unwrap());
    }
}
