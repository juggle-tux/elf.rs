// elf.rs
// Copyright (C) 2016  juggle-tux
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use {Addr32, Addr64};
use nom::{ErrorKind, IResult};
use parser::{Address, DataEnc, Offset, Parser, ParserError, Payload, Size};
use std::any::TypeId;
use std::fmt;
use std::io::SeekFrom;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Header<A> {
    pub ptype: Type,
    pub offset: A,
    pub phys_addr: A,
    pub virt_addr: A,
    pub file_size: A,
    pub mem_size: A,
    pub flags: Flags,
    pub align: A,
}

impl<A: Address> fmt::Display for Header<A> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        writeln!(fmt,
                 "type: {ty}\n\
                  file offset: {offset:#x}\n\
                  size: {size} bytes\talgin: {align} bytes\n\
                  virtual address: {virt:#x}\t physical address: {phys:#x}\n\
                  flags: {flags}",
                 ty = self.ptype,
                 offset = self.offset,
                 size = self.file_size,
                 virt = self.virt_addr,
                 phys = self.phys_addr,
                 align = self.align,
                 flags = self.flags)
    }
}

impl<A: Offset> Offset for Header<A> {
    fn offset(&self) -> SeekFrom {
        self.offset.offset()
    }
}

impl<A: Size> Size for Header<A> {
    fn size(&self) -> u64 {
        self.file_size.size()
    }
}

impl<A> Payload for Header<A>
    where A: Offset + Size
{}

impl<A: Address> Parser for Header<A> {
    fn parse(i: &[u8], enc: DataEnc) -> IResult<&[u8], Header<A>, ParserError> {
        match TypeId::of::<A>() {
            ty if ty == TypeId::of::<Addr32>() => parse_header32::<A>(i, enc),
            ty if ty == TypeId::of::<Addr64>() => parse_header64::<A>(i, enc),
            _ => unimplemented!()
        }
    }
}

fn parse_header32<A: Address>(i: &[u8], enc: DataEnc) -> IResult<&[u8], Header<A>, ParserError>  {
    add_return_error!(i,
        ErrorKind::Custom(ParserError::ProgramHeader),
        do_parse!(
            ptype: apply!(Type::parse, enc) >>
            offset: apply!(A::parse, enc) >>
            virt_addr: apply!(A::parse, enc) >>
            phys_addr: apply!(A::parse, enc) >>
            file_size: apply!(A::parse, enc) >>
            mem_size: apply!(A::parse, enc) >>
            flags: apply!(Flags::parse, enc) >>
            align: apply!(A::parse, enc) >>
            (
                Header {
                    ptype,
                    offset,
                    phys_addr,
                    virt_addr,
                    file_size,
                    mem_size,
                    flags,
                    align,
                }
            )
        )
    )
}

fn parse_header64<A: Address>(i: &[u8], enc: DataEnc) -> IResult<&[u8], Header<A>, ParserError> {
    add_return_error!(i,
        ErrorKind::Custom(ParserError::ProgramHeader),
        do_parse!(
            ptype: apply!(Type::parse, enc) >>
            flags: apply!(Flags::parse, enc) >>
            offset: apply!(A::parse, enc) >>
            virt_addr: apply!(A::parse, enc) >>
            phys_addr: apply!(A::parse, enc) >>
            file_size: apply!(A::parse, enc) >>
            mem_size: apply!(A::parse, enc) >>
            align: apply!(A::parse, enc) >>
            (
                Header {
                    ptype,
                    offset,
                    phys_addr,
                    virt_addr,
                    file_size,
                    mem_size,
                    flags,
                    align,
                }
            )
        )
    )
}

with_parser!(
    enum Type(u32::parse) where Err(ParserError::ProgramType) {
        0 => Null,
        1 => Load,
        2 => Dynamic,
        3 => Interpreter,
        4 => Note,
        5 => Shlib,
        6 => Header,
        7 => TLS,
        0x60000000...0x6fffffff => OSSpec(u32),
        0x70000000...0x7fffffff => ProcessorSpec(u32),
        _ => Unknnown(u32),
    }
);

impl fmt::Display for Type {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Type::OSSpec(ref n) => write!(fmt, "OS specific {:#x}", n),
            Type::ProcessorSpec(ref n) => write!(fmt, "Processor specific {:#x}", n),
            ty => write!(fmt, "{:?}", ty),
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Flags {
    pub read: bool,
    pub write: bool,
    pub execute: bool,
}

impl Parser for Flags {
    fn parse(i: &[u8], enc: DataEnc) -> IResult<&[u8], Self, ParserError> {
        add_return_error!(i, ErrorKind::Custom(ParserError::ProgramFlags),
            map!(apply!(u32::parse, enc), |n: u32| {
                Flags {
                    execute: n & (1 << 0) != 0,
                    write: n & (1 << 1) != 0,
                    read: n & (1 << 2) != 0,
                }
            })
        )
    }
}

impl fmt::Display for Flags {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        if self.read {
            write!(fmt, "R")?
        }
        if self.write {
            write!(fmt, "W")?
        }
        if self.execute {
            write!(fmt, "X")?
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use {Addr32, Addr64};
    use nom::IResult;
    use parser::{Address, DataEnc, Parser, ParserError};
    use parser::DataEnc::{BigEndian, LittleEndian};
    use super::{Flags, Header, Type};

    pub struct Test<A: Address> {
        pub enc: DataEnc,
        pub raw: &'static [u8],
        pub want: IResult<&'static [u8], Header<A>, ParserError>,
    }

    pub const TEST_I386: Test<Addr32> = Test{
        enc: LittleEndian,
        raw: include_bytes!(
            concat!(env!("CARGO_MANIFEST_DIR"), "/test_data/program_headers.i386")
        ),
        want: IResult::Done(&[], Header{
            ptype: Type::Header,
            offset: 52,
            phys_addr: 134512692,
            virt_addr: 134512692,
            file_size: 288,
            mem_size: 288,
            flags: Flags {
                read: true,
                write: false,
                execute: true,
            },
            align: 4,
        })
    };

    pub const TEST_AMD64: Test<Addr64> = Test{
        enc: LittleEndian,
        raw: include_bytes!(
            concat!(env!("CARGO_MANIFEST_DIR"), "/test_data/program_headers.amd64")
        ),
        want: IResult::Done(&[], Header {
            ptype: Type::Header,
            offset: 64,
            phys_addr: 4194368,
            virt_addr: 4194368,
            file_size: 504,
            mem_size: 504,
            flags: Flags {
                read: true,
                write: false,
                execute: true,
            },
            align: 8,
        })
    };

    pub const TEST_MIPS_BE: Test<Addr32> = Test{
        enc: BigEndian,
        raw: include_bytes!(
            concat!(env!("CARGO_MANIFEST_DIR"), "/test_data/program_headers.mips_be")
        ),
        want: IResult::Done(&[], Header {
            ptype: Type::Header,
            offset: 52,
            phys_addr: 4194356,
            virt_addr: 4194356,
            file_size: 384,
            mem_size: 384,
            flags: Flags {
                read: true,
                write: false,
                execute: true,
            },
            align: 4,
        })
    };

    #[test]
    fn parse_header() {
        macro_rules! test {
            ($( $test:expr ),+) => {$(
                print!("{} ... ", stringify!($test));
                test_parser($test);
                println!("ok");
            )+}
        }
        test!(TEST_I386, TEST_AMD64, TEST_MIPS_BE);
    }

    fn test_parser<A: Address>(test: Test<A>){
        let (_, got) = Header::parse(test.raw, test.enc).unwrap();
        let (_, want) = test.want.unwrap();
        assert_eq!(want, got)
    }
}

#[cfg(all(test, feature="benchmark"))]
mod benchmarks {
    use {Addr32, Addr64};
    use parser::Parser;
    use test::Bencher;
    use super::Header;
    use super::tests::{TEST_I386, TEST_AMD64, TEST_MIPS_BE};

    #[bench]
    fn parse_header_i386(b: &mut Bencher) {
        b.bytes = 32;
        b.iter(|| Header::<Addr32>::parse(TEST_I386.raw, TEST_I386.enc).unwrap());
    }

    #[bench]
    fn parse_header_amd64(b: &mut Bencher) {
        b.bytes = 64;
        b.iter(|| Header::<Addr64>::parse(TEST_AMD64.raw, TEST_AMD64.enc).unwrap());
    }

    #[bench]
    fn parse_header_mips_be(b: &mut Bencher) {
        b.bytes = 32;
        b.iter(|| Header::<Addr32>::parse(TEST_MIPS_BE.raw, TEST_MIPS_BE.enc).unwrap());
    }
}
