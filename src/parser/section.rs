// elf.rs
// Copyright (C) 2016  juggle-tux
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use nom::{ErrorKind, IResult};
use parser::{Address, DataEnc, Offset, Parser, ParserError, Payload, Size};
use std::fmt;
use std::io::SeekFrom;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Header<A> {
    pub name: Name,
    pub stype: Type,
    pub flags: Flags<A>,
    pub addr: A,
    pub offset: A,
    pub size: A,
    pub link: u32,
    pub info: u32,
    pub addr_algin: A,
    pub entry_size: A,
}

impl<A: Address> fmt::Display for Header<A> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        writeln!(fmt,
                 "type: {stype}\n\
                  address: {addr:#x}\n\
                  file offset: {offset:#x}\tsize: {size} bytes\n\
                  entry size: {e_size}\talign: {algin} bytes\n\
                  link: {link:#x}\tinfo: {info:#x}\n\
                  flags: {flags}",
                 stype = self.stype,
                 addr = self.addr,
                 offset = self.offset,
                 size = self.size,
                 e_size = self.entry_size,
                 algin = self.addr_algin,
                 link = self.link,
                 info = self.info,
                 flags = self.flags)
    }
}

impl<A: Offset> Offset for Header<A> {
    fn offset(&self) -> SeekFrom {
        self.offset.offset()
    }
}

impl<A: Size> Size for Header<A> {
    fn size(&self) -> u64 {
        self.size.size()
    }
}

impl<A> Payload for Header<A>
    where A: Offset + Size
{}

impl<A> Parser for Header<A>
    where A: Address + Into<u64>
{
    fn parse(i: &[u8], enc: DataEnc) -> IResult<&[u8], Self, ParserError> {
        add_return_error!(i,
            ErrorKind::Custom(ParserError::SectionHeader),
            do_parse!(
                name: apply!(Name::parse, enc) >>
                stype: apply!(Type::parse, enc) >>
                flags: apply!(Flags::parse, enc) >>
                addr: apply!(A::parse, enc) >>
                offset: apply!(A::parse, enc) >>
                size: apply!(A::parse, enc) >>
                link: apply!(u32::parse ,enc) >>
                info: apply!(u32::parse, enc) >>
                addr_algin: apply!(A::parse, enc) >>
                entry_size: apply!(A::parse, enc) >>
                (
                    Header {
                        name,
                        stype,
                        flags,
                        addr,
                        offset,
                        size,
                        link,
                        info,
                        addr_algin,
                        entry_size,
                    }
                )
            )
        )
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Name(pub u32);

impl Parser for Name {
    fn parse(i: &[u8], enc: DataEnc) -> IResult<&[u8], Name, ParserError> {
        add_return_error!(i,
            ErrorKind::Custom(ParserError::SectionName),
            map!(apply!(u32::parse, enc), Name)
        )
    }
}

with_parser!(
    enum Type(u32::parse) where Err(ParserError::SectionType) {
        0 => Null,
        1 => ProgramData,
        2 => SymbolTable,
        3 => StringTable,
        4 => RelocationWithAddens,
        5 => HashTable,
        6 => DynamicLinkInfo,
        7 => Note,
        8 => NoData,
        9 => RelocationNoAddens,
        10 => Shlib,
        11 => DynamicSymbolTable,
        12 => ConstructorArray,
        13 => DestructorArray,
        14 => PreConstrunctorArray,
        15 => SectionGroup,
        16 => ExtendedSectionIdx,
        0x6000_0000...0x6fff_ffff => OSSpecific(u32),
        0x7000_0000...0x7fff_ffff => ProcessorSpecific(u32),
        0x8000_0000...0x8fff_ffff => ApplicationSpecific(u32),
        _ => Unknown(u32),
    }
);

impl fmt::Display for Type {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Type::OSSpecific(ref n) => write!(fmt, "OS specific {:#x}", n),
            Type::ProcessorSpecific(ref n) => write!(fmt, "Processor specific {:#x}", n),
            Type::ApplicationSpecific(ref n) => write!(fmt, "Application Specific {:#x}", n),
            ty => write!(fmt, "{:?}", ty),
        }
    }
}


#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Flags<A> {
    pub write: bool,
    pub alloc: bool,
    pub execute: bool,
    pub mask: A,
}

impl<A> Parser for Flags<A>
    where A: Address + Into<u64>
{
    fn parse(i: &[u8], enc: DataEnc) -> IResult<&[u8], Self, ParserError> {
        add_return_error!(i,
            ErrorKind::Custom(ParserError::SectionFlags),
            map!(apply!(A::parse, enc), |n| {
                let nn: u64 = A::into(n);
                Flags {
                    write: nn & (1 << 0) != 0,
                    alloc: nn & (1 << 1) != 0,
                    execute: nn & (1 << 2) != 0,
                    mask: n
                }
            })
        )
    }
}

impl<A: Address> fmt::Display for Flags<A> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        if self.alloc {
            write!(fmt, "A")?
        }
        if self.write {
            write!(fmt, "W")?
        }
        if self.execute {
            write!(fmt, "X")?
        }
        if self.alloc || self.write || self.execute {
            write!(fmt, " ")?
        }
        write!(fmt, "({:#x})", self.mask)
    }
}

#[cfg(test)]
mod tests {
    use {Addr32, Addr64};
    use nom::IResult;
    use parser::{Address, Parser, ParserError};
    use parser::DataEnc::{self, BigEndian, LittleEndian};
    use super::{Flags, Header, Name, Type};

    pub struct Test<A> {
        pub enc: DataEnc,
        pub raw: &'static [u8],
        pub want: IResult<&'static [u8], Header<A>, ParserError>,
    }

    pub const TEST_I386: Test<Addr32> = Test{
        enc: LittleEndian,
        raw: include_bytes!(concat!(env!("CARGO_MANIFEST_DIR"), "/test_data/section_headers.i386")),
        want: IResult::Done(&[], Header {
            name: Name(0),
            stype: Type::Null,
            flags: Flags {
                write: false,
                alloc: false,
                execute: false,
                mask: 0,
            },
            addr: 0,
            offset: 0,
            size: 0,
            link: 0,
            info: 0,
            addr_algin: 0,
            entry_size: 0,
        })
    };

    pub const TEST_AMD64: Test<Addr64> = Test{
        enc: LittleEndian,
        raw: include_bytes!(concat!(env!("CARGO_MANIFEST_DIR"), "/test_data/section_headers.amd64")),
        want: IResult::Done(&[], Header {
            name: Name(0),
            stype: Type::Null,
            flags: Flags {
                write: false,
                alloc: false,
                execute: false,
                mask: 0,
            },
            addr: 0,
            offset: 0,
            size: 0,
            link: 0,
            info: 0,
            addr_algin: 0,
            entry_size: 0,
        }),
    };

    pub const TEST_MIPS_BE: Test<Addr32> = Test{
        enc: BigEndian,
        raw: include_bytes!(concat!(env!("CARGO_MANIFEST_DIR"), "/test_data/section_headers.mips_be")),
        want: IResult::Done(&[], Header {
            name: Name(0),
            stype: Type::Null,
            flags: Flags {
                write: false,
                alloc: false,
                execute: false,
                mask: 0,
            },
            addr: 0,
            offset: 0,
            size: 0,
            link: 0,
            info: 0,
            addr_algin: 0,
            entry_size: 0,
        }),
    };

    #[test]
    fn parse_header() {
        macro_rules! test {
            ($( $test:expr ),+) => {$(
                print!("{} ... ", stringify!($test));
                test_parser($test);
                println!("ok");
            )+}
        }
        test!(TEST_I386, TEST_AMD64, TEST_MIPS_BE);
    }

    fn test_parser<A>(test: Test<A>)
        where A: Address + Into<u64>
    {
        let (_, got) = Header::parse(test.raw, test.enc).unwrap();
        let (_, want) = test.want.unwrap();
        assert_eq!(want, got)
    }
 }

#[cfg(all(test, feature="benchmark"))]
mod benchmarks {
    use {Addr32, Addr64};
    use parser::Parser;
    use test::Bencher;
    use super::Header;
    use super::tests::{TEST_I386, TEST_AMD64, TEST_MIPS_BE};

    #[bench]
    fn parse_header_i386(b: &mut Bencher) {
        b.bytes = 40;
        b.iter(|| Header::<Addr32>::parse(TEST_I386.raw, TEST_I386.enc).unwrap());
    }

    #[bench]
    fn parse_header_amd64(b: &mut Bencher) {
        b.bytes = 64;
        b.iter(|| Header::<Addr64>::parse(TEST_AMD64.raw, TEST_AMD64.enc).unwrap());
    }

    #[bench]
    fn parse_header_mips_be(b: &mut Bencher) {
        b.bytes = 40;
        b.iter(|| Header::<Addr32>::parse(TEST_MIPS_BE.raw, TEST_MIPS_BE.enc).unwrap());
    }
}
