use nom::{ErrorKind, IResult};
use parser::{Address, HeaderTable, Ident, Parser, ParserError, Version};
use std::fmt;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Header<A> {
    pub ident: Ident,
    pub etype: Type,
    pub machine: Machine,
    pub version: Version,
    pub entry: A,
    pub flags: u32,
    pub size: u16,
    pub program_headers: HeaderTable<A>,
    pub section_table: HeaderTable<A>,
    pub str_idx: u16,
}

impl<A: Address> Header<A> {
    pub fn parse(i: &[u8], ident: Ident) -> IResult<&[u8], Self, ParserError> {
        let enc = ident.data_enc;
        add_return_error!(i,
            ErrorKind::Custom(ParserError::ElfHeader),
            do_parse!(
                etype: apply!(Type::parse, enc) >>
                machine: apply!(Machine::parse, enc) >>
                version: apply!(<Version as Parser>::parse, enc) >>
                entry: apply!(A::parse, enc) >>
                ph_off: apply!(A::parse, enc) >>
                sh_off: apply!(A::parse, enc) >>
                flags: apply!(u32::parse, enc) >>
                size: apply!(u16::parse, enc) >>
                ph_ent_size: apply!(u16::parse, enc) >>
                ph_num: apply!(u16::parse, enc) >>
                sh_ent_size: apply!(u16::parse, enc) >>
                sh_num: apply!(u16::parse, enc) >>
                str_idx: apply!(u16::parse, enc) >>
                (
                    Header {
                        ident,
                        etype,
                        machine,
                        version,
                        entry,
                        flags,
                        size,
                        program_headers: HeaderTable {
                            offset: ph_off,
                            entry_size: ph_ent_size,
                            amount: ph_num,
                        },
                        section_table: HeaderTable {
                            offset: sh_off,
                            entry_size: sh_ent_size,
                            amount: sh_num,
                        },
                        str_idx,
                    }
                )
            ))
    }
}

impl<A: Address> fmt::Display for Header<A> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        writeln!(fmt,
                 "{ident}\n\
                  type: {ty}\n\
                  arch: {machine}\n\
                  flags: {flags:#x}\n\
                  entry: {entry:#x}\n\
                  header size: {size} bytes\n\
                  program headers: {ph_num} x {ph_ent_s} bytes @ {ph_off:#x}\n\
                  section headers: {sh_num} x {sh_ent_s} bytes @ {sh_off:#x}",
                 ident = self.ident,
                 ty = self.etype,
                 machine = self.machine,
                 entry = self.entry,
                 flags = self.flags,
                 size = self.size,
                 ph_num = self.program_headers.amount,
                 ph_off = self.program_headers.offset,
                 ph_ent_s = self.program_headers.entry_size,
                 sh_num = self.section_table.amount,
                 sh_off = self.section_table.offset,
                 sh_ent_s = self.section_table.entry_size)
    }
}

with_parser!(
    enum Type(u16::parse) where Err(ParserError::ElfType) {
        0 => None,
        1 => Relocatable,
        2 => Executable,
        3 => Shared,
        4 => Core,
        0xfe00...0xfeff => OSSpec(u8),
        0xff00...0xffff => ProcessorSpec(u8),
        _ => Unknown(u16),
    }
);

impl fmt::Display for Type {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Type::OSSpec(ref n) => write!(fmt, "OS specific {:#x}", n),
            Type::ProcessorSpec(ref n) => write!(fmt, "Processor specific {:#x}", n),
            ty => write!(fmt, "{:?}", ty),
        }
    }
}

with_parser!(
    #[allow(non_camel_case_types)]
    enum Machine(u16::parse) where Err(ParserError::Machine) {
        0 => None,
        1 => M32,
        2 => SPARC,
        3 => I386,
        4 => M68k,
        5 => M88k,
        7 => I860,
        8 => MIPS,
        9 => S370,
        10 => MIPS_RS3_LE,

        15 => PARISC,

        17 => VPP500,
        18 => SPARC32PLUS,
        19 => I960,
        20 => PPC,
        21 => PPC64,
        22 => S390,

        36 => V800,
        37 => FR20,
        38 => RH32,
        39 => RCE,
        40 => ARM,
        41 => FAKE_ALPHA,
        42 => SH,
        43 => SPARCV9,
        44 => TRICORE,
        45 => ARC,
        46 => H8_300,
        47 => H8_300H,
        48 => H8S,
        49 => H8_500,
        50 => IA_64,
        51 => MIPS_X,
        52 => COLDFIRE,
        53 => M68HC12,
        54 => MMA,
        55 => PCP,
        56 => NCPU,
        57 => NDR1,
        58 => STARCORE,
        59 => ME16,
        60 => ST100,
        61 => TINYJ,
        62 => X86_64,
        63 => PDSP,

        66 => FX66,
        67 => ST9PLUS,
        68 => ST7,
        69 => M68HC16,
        70 => M68HC11,
        71 => M68HC08,
        72 => M68HC05,
        73 => SVX,
        74 => ST19,
        75 => VAX,
        76 => CRIS,
        77 => JAVELIN,
        78 => FIREPATH,
        79 => ZSP,
        80 => MMIX,
        81 => HUANY,
        82 => PRISM,
        83 => AVR,
        84 => FR30,
        85 => D10V,
        86 => D30V,
        87 => V850,
        88 => M32R,
        89 => MN10300,
        90 => MN10200,
        91 => PJ,
        92 => OPENRISC,
        93 => ARC_A5,
        94 => XTENSA,

        183 => AARCH64,

        188 => TILEPRO,
        189 => MICROBLAZE,

        191 => TILEGX,

        _ => Unknown(u16),
    }
);

impl fmt::Display for Machine {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{:?}", self)
    }
}

#[cfg(test)]
mod tests {
    use {Addr32, Addr64};
    use nom::IResult;
    use parser::{Abi, AbiKind, Address, Class, DataEnc, HeaderTable, Ident, ParserError, Version};
    use super::{Header, Machine, Type};

    pub struct Test<A> {
        pub raw: &'static [u8],
        pub want: IResult<&'static [u8], Header<A>, ParserError>
    }

    pub const TEST_I386: Test<Addr32> = Test{
        raw: include_bytes!(concat!(env!("CARGO_MANIFEST_DIR"), "/test_data/elf_header.i386")),
        want: IResult::Done(&[], Header {
            ident: Ident {
                class: Class::Elf32,
                data_enc: DataEnc::LittleEndian,
                elf_version: Version::Current,
                os_abi: Abi {
                    kind: AbiKind::SYSV,
                    version: 0,
                },
            },
            etype: Type::Executable,
            machine: Machine::I386,
            version: Version::Current,
            entry: 134529823,
            flags: 0,
            size: 52,
            program_headers: HeaderTable {
                offset: 52,
                entry_size: 32,
                amount: 9,
            },
            section_table: HeaderTable {
                offset: 355700,
                entry_size: 40,
                amount: 27,
            },
            str_idx: 26,
        }),
    };

    pub const TEST_AMD64: Test<Addr64> = Test{
        raw: include_bytes!(concat!(env!("CARGO_MANIFEST_DIR"), "/test_data/elf_header.amd64")),
        want: IResult::Done(&[], Header {
            ident: Ident {
                class: Class::Elf64,
                data_enc: DataEnc::LittleEndian,
                elf_version: Version::Current,
                os_abi: Abi {
                    kind: AbiKind::SYSV,
                    version: 0,
                },
            },
            etype: Type::Shared,
            machine: Machine::X86_64,
            version: Version::Current,
            entry: 45488,
            flags: 0,
            size: 64,
            program_headers: HeaderTable {
                offset: 64,
                entry_size: 56,
                amount: 9,
            },
            section_table: HeaderTable {
                offset: 5502272,
                entry_size: 64,
                amount: 46,
            },
            str_idx: 43,
        }),
    };

    pub const TEST_MIPS_BE: Test<Addr32> = Test{
        raw: include_bytes!(concat!(env!("CARGO_MANIFEST_DIR"), "/test_data/elf_header.mips_be")),
        want: IResult::Done(&[], Header {
            ident: Ident {
                class: Class::Elf32,
                data_enc: DataEnc::BigEndian,
                elf_version: Version::Current,
                os_abi: Abi {
                    kind: AbiKind::SYSV,
                    version: 0,
                },
            },
            etype: Type::Executable,
            machine: Machine::MIPS,
            version: Version::Current,
            entry: 4216768,
            flags: 268439559,
            size: 52,
            program_headers: HeaderTable {
                offset: 52,
                entry_size: 32,
                amount: 12,
            },
            section_table: HeaderTable {
                offset: 378212,
                entry_size: 40,
                amount: 33,
            },
            str_idx: 32,
        }),
    };

    #[test]
    fn parse_header() {
        macro_rules! test {
            ($( $test:expr ),+) => {$(
                print!("{} ... ", stringify!($test));
                test_parser($test);
                println!("ok");
            )+}
        }
        test!(TEST_I386, TEST_AMD64, TEST_MIPS_BE);
    }

    fn test_parser<A>(test: Test<A>)
        where A: Address
    {
        let (raw, ident) = Ident::parse(test.raw).unwrap();
        let (_, got) = Header::parse(raw, ident).unwrap();
        let (_, want) = test.want.unwrap();
        assert_eq!(want, got)
    }

}

#[cfg(all(test, feature="benchmark"))]
mod benchmarks {
    use super::Header;
    use super::tests::{TEST_I386, TEST_AMD64, TEST_MIPS_BE};
    use {Addr32, Addr64};
    use parser::Ident;

    use test::Bencher;

    #[bench]
    fn parse_header_i386(b: &mut Bencher) {
        b.bytes = elf32_head_size!() - ident_size!();
        let (raw, ident) = Ident::parse(TEST_I386.raw).unwrap();
        b.iter(|| Header::<Addr32>::parse(raw, ident).unwrap())
    }

    #[bench]
    fn parse_header_mips_be(b: &mut Bencher) {
        b.bytes = elf32_head_size!() - ident_size!();
        let (raw, ident) = Ident::parse(TEST_MIPS_BE.raw).unwrap();
        b.iter(|| Header::<Addr32>::parse(raw, ident).unwrap())
    }

    #[bench]
    fn parse_header_amd64(b: &mut Bencher) {
        b.bytes = elf64_head_size!() - ident_size!();
        let (raw, ident) = Ident::parse(TEST_AMD64.raw).unwrap();
        b.iter(|| Header::<Addr64>::parse(raw, ident).unwrap())
    }
}
