macro_rules! build_enum {
    (
        $(#[$attr:meta])*
        enum $name:ident {
            $($variant:ident$(($ty:ty))*,)+
        }
    ) => {
        $(#[$attr])*
        #[derive(Clone, Copy, Debug, PartialEq, Eq)]
        pub enum $name {
            $($variant$(($ty))*,)+
        }
    }
}

macro_rules! impl_parser {
    // parse single byte
    (
        for $name:ident where $(@ $invalid:pat =>)* Err($error:expr) {
            $( $p:pat => $variant:ident$(($ty:ty))*,)+
        }
    ) => {
        impl $name {
            pub fn parse(i: &[u8])
            -> ::nom::IResult<&[u8], $name, ::parser::ParserError> {
                add_return_error!(i, ::nom::ErrorKind::Custom($error),
                    map_opt!(call!(::parser::byte),
                        |n| match n {
                            $($p => Some($name::$variant$((n as $ty))*),)+
                                $( $invalid => { None } )*
                        }
                    )
                )
            }
        }
    };

    // parse multi byte with DataEnc
    (
        for $name:ident($addr:expr) where $(@ $invalid:pat =>)* Err($error:expr) {
            $( $p:pat => $variant:ident$(($ty:ty))*,)+
        }
    ) => {
        impl ::parser::Parser for $name {
            fn parse(i: &[u8], enc: ::parser::DataEnc)
            -> IResult<&[u8], $name, ::parser::ParserError> {
                add_return_error!(i, ::nom::ErrorKind::Custom($error),
                    map_opt!(apply!($addr, enc),
                        |n| match n {
                            $($p => Some($name::$variant$((n as $ty))*),)+
                                $( $invalid => { None } )*
                        }
                    )
                )
            }
        }
    };
}

macro_rules! with_parser {
    (
        $(#[$attr:meta])*
        enum $name:ident$(($parser:expr))* where $(@ $invalid:pat =>)* Err($error:expr) {
            $($pat:pat => $variant:ident$(($ty:ty))*,)+
        }
    ) => {
        build_enum!($(#[$attr])* enum $name { $($variant$(($ty))*,)+ });
        impl_parser!(for $name$(($parser))* where $(@ $invalid =>)* Err($error) {
            $( $pat => $variant$(($ty))*,)+
        });
    };
}
