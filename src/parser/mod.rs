// elf.rs
// Copyright (C) 2016  juggle-tux
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use nom::{self, IResult};
use std::any::Any;
use std::fmt;
use std::io::SeekFrom;

#[macro_use] mod macros;
mod elf;
mod ident;
mod program;
mod section;
mod symbol;
mod error;

pub use self::elf::Header as ElfHeader;
pub use self::elf::Machine;
pub use self::elf::Type as ElfType;
pub use self::error::ParserError;
pub use self::ident::{Abi, AbiKind, AbiVersion, Class, DataEnc, Ident, Version};
pub use self::program::Flags as ProgramFlags;
pub use self::program::Header as ProgramHeader;
pub use self::program::Type as ProgramType;
pub use self::section::Flags as SectionFlags;
pub use self::section::Header as SectionHeader;
pub use self::section::Name;
pub use self::section::Type as SectionType;
pub use self::symbol::Symbol;

pub trait Parser: Sized {
    fn parse(i: &[u8], enc: DataEnc) -> IResult<&[u8], Self, ParserError>;
}

pub trait Address
    : Any + Copy + Eq + Parser + PartialEq + fmt::Debug + fmt::Display + fmt::LowerHex
{}

pub trait Offset {
    fn offset(&self) -> SeekFrom;
}

pub trait Size {
    fn size(&self) -> u64;
}

pub trait Payload: Offset + Size {}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct HeaderTable<O>
{
    pub offset: O,
    pub entry_size: u16,
    pub amount: u16,
}

impl<O: Offset> Offset for HeaderTable<O> {
    fn offset(&self) -> SeekFrom {
        self.offset.offset()
    }
}

impl<O> Size for HeaderTable<O> {
    fn size(&self) -> u64 {
        self.entry_size as u64 * self.amount as u64
    }
}

impl<O: Offset> Payload for HeaderTable<O> {}

named!(byte<&[u8], u8, ParserError>, fix_error!(ParserError, map!(take!(1), |b: &[u8]| b[0])));

macro_rules! make_primitive {
    ($ty:ty, $le_func:expr, $be_func:expr) => {
        impl Parser for $ty {
            fn parse(i: &[u8], enc: DataEnc) -> IResult<&[u8], $ty, ParserError> {
                use self::DataEnc::*;
                match enc {
                    LittleEndian => fix_error!(i, ParserError, call!($le_func)),
                    BigEndian => fix_error!(i, ParserError, call!($be_func)),
                }
            }
        }
    }
}

make_primitive!(u16, nom::le_u16, nom::be_u16);
make_primitive!(u32, nom::le_u32, nom::be_u32);
make_primitive!(u64, nom::le_u64, nom::be_u64);
