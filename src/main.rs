#![cfg(feature="cli")]
#[macro_use]
extern crate clap;

#[macro_use]
extern crate log;

extern crate env_logger;
extern crate elf;

use clap::ArgMatches;
use elf::{Elf, ReadElf, Result};
use elf::file::Headers;
use elf::parser::Address;
use env_logger::{LogBuilder, LogTarget};
use log::LogLevelFilter;
use std::fs::File;

fn main() {
    let args = clap_app!(
        (crate_name!()) =>
            (version: (crate_version!()))
            (author: (crate_authors!()))
            (about: (crate_description!()))
            (@arg file: * "Path to elf file to use")
            (@arg print_header: -e --elf "Prints the elf header")
            (@arg print_sections: -s --sections "Prints the section headers")
            (@arg print_programs: -p --programs "Prints the program headers")
            (@arg verbose: -v --verbose ... "Sets the level of verbosity")
            (@arg quite: -q --quite "Be quite")
    ).get_matches();

    LogBuilder::new()
        .target(LogTarget::Stderr)
        .filter(None, {
            match args.occurrences_of("verbose") {
                0 if args.is_present("quite") => LogLevelFilter::Off,
                0 => LogLevelFilter::Error,
                1 => LogLevelFilter::Info,
                2 => LogLevelFilter::Debug,
                _ => LogLevelFilter::Trace,
            }
        })
        .init().unwrap();

    run(args).unwrap_or_else(|err| {
        error!("{}", err);
        ::std::process::exit(1)
    })
}

fn run(args: ArgMatches) -> Result<()> {
    info!("Running {} {}", crate_name!(), crate_version!());
    File::open(args.value_of("file").unwrap())
        .map_err(Into::into)
        .and_then(|mut file| file.read_elf())
        .map(|elf| {
            let e = args.is_present("print_header");
            let s = args.is_present("print_sections");
            let p =  args.is_present("print_programms");
            match elf {
                Elf::File32(ref headers) => print_headers(headers, e, p, s),
                Elf::File64(ref headers) => print_headers(headers, e, p ,s),
            }
        })
}

fn print_headers<A: Address>(
    headers: &Headers<A>,
    elf: bool,
    program: bool,
    section: bool
) {
    if elf {
        println!("{}", &headers.elf_header);
    }

    if program {
        println!("program headers:");
        for (idx, ph) in headers.program_table.iter().enumerate() {
            println!("[{}]\n{}", idx, ph)
        }
    }

    if section {
        println!("section headers:");
        for (idx, (name, sh)) in headers.section_table.iter().enumerate() {
            println!("[{}]: {:?}\n{}", idx, name, sh)
        }
    }
}
