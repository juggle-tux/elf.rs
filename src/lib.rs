// elf.rs
// Copyright (C) 2016  juggle-tux
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

//! Elf file parser
//!
//! # Examples
//!
//! Open file:
//!
//! ```
//! # use std::fs::File;
//! # let path = concat!(env!("CARGO_MANIFEST_DIR"), "/test_data/objdump.i386");
//! # let file = &mut File::open(path).unwrap();
//!
//! use elf::ReadElf;
//! let elf = file.read_elf().unwrap();
//! println!("{:#?}", elf);
//! ```
#![allow(unknown_lints)]
#![warn(missing_copy_implementations, missing_debug_implementations,
        unused_import_braces)]
#![deny(unsafe_code, unused_extern_crates)]
#![cfg_attr(all(test, feature="benchmark"), feature(test))]

#[macro_use]
extern crate nom;

#[macro_use]
extern crate from_variants;

#[cfg(test)]
#[macro_use]
extern crate pretty_assertions;

#[cfg(all(test, feature="benchmark"))]
extern crate test;

mod error;

use file::Headers;
use std::io::{Read, Seek, SeekFrom};
use parser::{Address, Offset, Size};

macro_rules! ident_size {
    () => {16}
}
macro_rules! elf32_head_size {
    () => {52}
}
macro_rules! elf64_head_size {
    () => {64}
}

macro_rules! error_node_position(
    ($code:expr, $input: expr, $next:expr) => (
        {
            let (_, _) = ($input, $next);
            $code
        }
    );
);

pub mod parser;
pub mod file;

pub use error::Error;

pub type Addr32 = u32;
impl Address for Addr32 {}
impl Offset for Addr32 {
    fn offset(&self) -> SeekFrom {
        SeekFrom::Start(*self as u64)
    }
}
impl Size for Addr32 {
    fn size(&self) -> u64 {
        *self as u64
    }
}

pub type Addr64 = u64;
impl Address for Addr64 {}
impl Offset for Addr64 {
    fn offset(&self) -> SeekFrom {
        SeekFrom::Start(*self)
    }
}
impl Size for Addr64 {
    fn size(&self) -> u64 {
        *self
    }
}

pub type Result<T> = std::result::Result<T, Error>;

#[allow(enum_variant_names)]
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Elf {
    File32(Headers<Addr32>),
    File64(Headers<Addr64>),
}

pub trait ReadElf: Read + Seek {
    fn read_elf(&mut self) -> Result<Elf>;
}

impl<T> ReadElf for T
    where T: Read + Seek
{
    fn read_elf(&mut self) -> Result<Elf> {
        file::Reader::new(self).and_then(|mut r| r.headers())
    }
}

#[cfg(test)]
mod tests {
    use {Elf, ReadElf};
    use std::io;

    pub const RAW_FILE_I386: &[u8] = include_bytes!(concat!(env!("CARGO_MANIFEST_DIR"),
                                                                    "/test_data/objdump.i386"));
    pub const RAW_FILE_AMD64: &[u8] = include_bytes!(concat!(env!("CARGO_MANIFEST_DIR"),
                                                                     "/test_data/objdump.amd64"));
    pub const RAW_FILE_MIPS_BE: &[u8] = include_bytes!(concat!(env!("CARGO_MANIFEST_DIR"),
                                                                       "/test_data/objdump.mips_be"));

    #[test]
    fn read_file_i386() {
        io::Cursor::new(RAW_FILE_I386)
            .read_elf()
            .map(|elf| match elf {
                Elf::File32(_) => (),
                e => panic!("expected Elf32 got {:?}", e),
            })
            .unwrap()
    }

    #[test]
    fn read_file_amd64() {
        io::Cursor::new(RAW_FILE_AMD64)
            .read_elf()
            .map(|elf| match elf {
                Elf::File64(_) => (),
                e => panic!("expected Elf64 got {:?}", e),
            })
            .unwrap()
    }

    #[test]
    fn read_file_mips_be() {
        io::Cursor::new(RAW_FILE_MIPS_BE)
            .read_elf()
            .map(|elf| match elf {
                Elf::File32(_) => (),
                e => panic!("expected Elf32 got {:?}", e),
            })
            .unwrap()
    }
}

#[cfg(all(test, feature="benchmark"))]
mod benchmarks {
    use {Elf, ReadElf};
    use parser::Size;
    use std::io;
    use super::tests;
    use test::Bencher;

    #[bench]
    fn read_file_i386(b: &mut Bencher) {
        let file = &mut io::Cursor::new(tests::RAW_FILE_I386);
        if let Elf::File32(elf) = file.read_elf().unwrap() {
            let st_size = elf.elf_header.section_table.size();
            let pt_size = elf.elf_header.program_headers.size();
            b.bytes = elf64_head_size!() + st_size + pt_size;
            b.iter(|| file.read_elf().unwrap());
        }
    }

    #[bench]
    fn read_file_amd64(b: &mut Bencher) {
        let file = &mut io::Cursor::new(tests::RAW_FILE_AMD64);
        if let Elf::File64(elf) = file.read_elf().unwrap() {
            let st_size = elf.elf_header.section_table.size();
            let pt_size = elf.elf_header.program_headers.size();
            b.bytes = elf64_head_size!() + st_size + pt_size;
            b.iter(|| file.read_elf().unwrap());
        }
    }

    #[bench]
    fn read_file_mips_be(b: &mut Bencher) {
        let file = &mut io::Cursor::new(tests::RAW_FILE_MIPS_BE);
        if let Elf::File32(elf) = file.read_elf().unwrap() {
            let st_size = elf.elf_header.section_table.size();
            let pt_size = elf.elf_header.program_headers.size();
            b.bytes = elf64_head_size!() + st_size + pt_size;
            b.iter(|| file.read_elf().unwrap());
        }
    }
}
