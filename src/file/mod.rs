// elf.rs
// Copyright (C) 2016  juggle-tux
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use {Elf, Error, Result};
use parser::{Address, Class, ElfHeader, HeaderTable, Ident, Name, Offset, Parser,
             Payload, ProgramHeader, SectionHeader, Size};
use std::borrow::Cow;
use std::collections::HashMap;
use std::fmt;
use std::io::{Read, Seek, SeekFrom};
use std::ops::{Deref, DerefMut};

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Headers<A> {
    pub elf_header: ElfHeader<A>,
    pub program_table: ProgramTable<A>,
    pub section_table: SectionTable<A>,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct SectionTable<A> {
    headers: Box<[SectionHeader<A>]>,
    str_table: StringTable,
}

impl<A> SectionTable<A> {
    pub fn get(&self, idx: u16) -> Option<(Cow<str>, &SectionHeader<A>)> {
        self.get_header(idx)
            .map(|header| (self.section_name(header), header))
    }

    pub fn get_name(&self, idx: u16) -> Option<Cow<str>> {
        self.get_header(idx).map(|header| self.section_name(header))
    }

    pub fn get_header(&self, idx: u16) -> Option<&SectionHeader<A>> {
        self.headers.get(idx as usize)
    }

    pub fn get_header_by_name(&self, name: &str) -> Option<&SectionHeader<A>> {
        self.iter()
            .filter(|&(ref header_name, _)| header_name == name)
            .map(|(_, header)| header)
            .next()
    }

    pub fn section_name(&self, header: &SectionHeader<A>) -> Cow<str> {
        self.str_table.get(header.name)
    }

    pub fn hash_map(&self) -> HashMap<Cow<str>, &SectionHeader<A>> {
        self.iter().collect()
    }

    pub fn iter(&self) -> SectionIter<A> {
        SectionIter{
            iter: self.headers.iter(),
            strs: &self.str_table,
        }
    }
}

impl<A> Deref for SectionTable<A> {
    type Target = [SectionHeader<A>];
    fn deref(&self) -> &Self::Target {
        &*self.headers
    }
}

impl<'a, A: 'a> IntoIterator for &'a SectionTable<A> {
    type Item = (Cow<'a, str>, &'a SectionHeader<A>);
    type IntoIter = SectionIter<'a, A>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

#[derive(Clone, Debug)]
pub struct SectionIter<'a, A: 'a> {
    iter: ::std::slice::Iter<'a, SectionHeader<A>>,
    strs: &'a StringTable
}

impl<'a, A: 'a> Iterator for SectionIter<'a, A> {
    type Item = (Cow<'a, str>, &'a SectionHeader<A>);

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(|header| (self.strs.get(header.name), header))
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.iter.size_hint()
    }

    fn count(self) -> usize {
        self.iter.count()
    }

    fn nth(&mut self, n: usize) -> Option<Self::Item> {
        self.iter.nth(n).map(|header| (self.strs.get(header.name), header))
    }

    fn last(self) -> Option<Self::Item> {
        let strs = self.strs;
        self.iter.last().map(|header| (strs.get(header.name), header))
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct ProgramTable<A> {
    headers: Box<[ProgramHeader<A>]>,
}

impl<A> ProgramTable<A> {
    pub fn get(&self, idx: u16) -> Option<&ProgramHeader<A>> {
        self.headers.get(idx as usize)
    }
}

impl<A> Deref for ProgramTable<A> {
    type Target = [ProgramHeader<A>];
    fn deref(&self) -> &Self::Target {
        &*self.headers
    }
}

#[derive(Clone, PartialEq, Eq)]
pub struct StringTable(Box<[u8]>);
impl StringTable {
    pub fn get(&self, name: Name) -> Cow<str> {
        let start = name.0 as usize;
        let buf = match self.0[start..].iter().position(|&b| b == 0) {
            Some(len) => &self.0[start .. start + len],
            None => &self.0[start..]
        };
        String::from_utf8_lossy(buf)
    }
}

impl fmt::Debug for StringTable {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_list()
            .entries(self.0.split(|b| *b == 0).map(String::from_utf8_lossy))
            .finish()
    }
}

#[derive(Copy, Clone, Debug)]
pub struct Reader<F> {
    file: F,
    ident: Ident,
}

impl<F> Deref for Reader<F> {
    type Target = F;
    fn deref(&self) -> &Self::Target {
        &self.file
    }
}
impl<F> DerefMut for Reader<F> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.file
    }
}

impl<F> Reader<F>
    where F: Read + Seek
{
    pub fn new(mut file: F) -> Result<Self> {
        let buf = &mut [0; 16];
        file.seek(SeekFrom::Start(0))?;
        file.read_exact(buf)?;
        let ident = Ident::parse(buf).to_full_result()?;
        Ok(Reader {file, ident})
    }

    pub fn headers(&mut self) -> Result<Elf> {
        match self.ident.class {
            Class::Elf32 => self.read_headers().map(|headers| Elf::File32(headers)),
            Class::Elf64 => self.read_headers().map(|headers| Elf::File64(headers)),
        }
    }

    fn read_headers<A>(&mut self) -> Result<Headers<A>>
        where A: Address + Offset + Size + Into<u64>
    {
        let elf_header = self.read_elf_header()?;
        let section_headers = self.headers_from_table(&elf_header.section_table)?;
        let str_table = section_headers[elf_header.str_idx as usize];
        Ok(Headers {
            elf_header,
            program_table: ProgramTable{
                headers: self.headers_from_table(&elf_header.program_headers)?,
            },
            section_table: SectionTable{
                headers: section_headers,
                str_table: StringTable(self.read_payload(&str_table)?),
            }
        })
    }

    fn read_payload(&mut self, data: &Payload) -> Result<Box<[u8]>> {
        let mut buf = vec![0; data.size() as usize];
        self.seek(data.offset())?;
        self.read_exact(&mut buf)?;
        Ok(buf.into_boxed_slice())
    }

    fn read_elf_header<A: Address>(&mut self) -> Result<ElfHeader<A>> {
        // we can safely use the elf64 size for all file since they
        // nead atleast one program- or sectionheader booth are bigger
        // as the difference betwean the Elf32 and Elf64
        let buf = &mut [0; elf64_head_size!() - ident_size!()];
        self.file.seek(SeekFrom::Start(ident_size!()))?;
        self.file.read_exact(buf)?;
        ElfHeader::parse(buf, self.ident).to_full_result().map_err(Error::from)
    }

    fn headers_from_table<A, P>(&mut self, ht: &HeaderTable<A>)
        -> Result<Box<[P]>>
        where P: Parser,
              A: Address + Offset
    {
        let mut buf = &*self.read_payload(ht)?;
        let mut headers = Vec::with_capacity(ht.amount as usize);

        for _ in 0..ht.amount {
            P::parse(buf, self.ident.data_enc)
                .to_full_result()
                .map(|header| headers.push(header))?;
            buf = &buf[ht.entry_size as usize..];
        }
        Ok(headers.into_boxed_slice())
    }
}
