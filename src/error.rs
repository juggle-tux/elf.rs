use nom::{ErrorKind, IError};
use parser::ParserError;
use std::error::Error as StdError;
use std::fmt;
use std::io;
use std::str::Utf8Error;

#[derive(Debug, FromVariants)]
pub enum Error {
    Io(io::Error),
    Parser(ErrorKind<ParserError>),
    Utf8(Utf8Error),
}


impl StdError for Error {
    fn description(&self) -> &str {
        match *self {
            Error::Io(_) => "io error",
            Error::Parser(_) => "parsing error",
            Error::Utf8(_) => "utf8 error",
        }
    }

    fn cause(&self) -> Option<&StdError> {
        match *self {
            Error::Io(ref e) => Some(e),
            Error::Parser(ref e) => Some(e),
            Error::Utf8(ref e) => Some(e),
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::Io(ref e) => write!(f, "io: {}", e),
            Error::Parser(ref e) => match *e {
                ErrorKind::Custom(ref e) => write!(f, "parsing: {:#?}", e),
                ref e => write!(f, "parsing: {:#?}", e),
            },
            Error::Utf8(ref e) => write!(f, "utf8: {}", e),
        }
    }
}

impl From<IError<ParserError>> for Error {
    fn from(e: IError<ParserError>) -> Self {
        match e {
            IError::Incomplete(_) => Error::Parser(ErrorKind::Custom(ParserError::NotEnoughData)),
            IError::Error(e) => Error::Parser(e),
        }
    }
}
